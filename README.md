# B2 EFREI Cyber 2023 Gestion d'infrastructure Cloud

> *c lon kom titr*

Ici vous trouverez tous les supports de cours, TPs et autres ressources liées au cours.

# TPs

- **[TP1 : Programmatic provisioning](./tp/1/README.md)**
  - premiers pas sur l'automatisation de déploiements de VMs avec Vagrant + Cloud-init
- **[TP1.5 : Proposer une image renforcée](./tp/1.5/README.md)**
  - on va un peu plus loin avec Vagrant, et on propose une image de base, avec une politique de sécu renforcée
- **[TP2 : Network boot](./tp/2/README.md)**
  - TP court autour de PXE : élément indispensable de toute grosse infra
- **[TP3 : Self-hosted private cloud platform](./tp/3/README.md)**
  - on monte une plateforme de cloud privé, pour avoir des features équivalentes à un AWS, mais hébérgé en interne
- **[TP4 : Automatisation et gestion de conf](./tp/4/README.md)**
  - gestion et déploiement de confs avec Ansible
  - big fat combo : Vagrant + Cloud-init + Ansible

![Cloud](./img/cloud.png)
